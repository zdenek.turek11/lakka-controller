import fcntl


def lock(name):
    fl = open(name, 'w+')
    try:
        fcntl.flock(fl, fcntl.LOCK_EX | fcntl.LOCK_NB)
        return True
    except IOError as e:
        return False


def unlock(name):
    fl = open(name, 'w+')
    fcntl.flock(fl, fcntl.LOCK_UN)


def test(name):
    if lock(name):
        unlock(name)
        return True
    else:
        return False
