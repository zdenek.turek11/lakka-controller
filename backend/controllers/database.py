#!/usr/bin/python2.7

import ConfigParser
import os.path
import fcntl
from locker import test as lockTest

fl = open('../locks/db.lock', 'w+')


def check():
    config = ConfigParser.ConfigParser()
    if os.path.exists('../config/config.ini'):
        config.read('../config/config.ini')
    else:
        print 'Config file not exists'
        exit(-1)

    if not lockTest('../locks/db.lock'):
        print 'Processing'
        exit(1)

    if os.path.exists(config.get('DB', 'file')):
        print 'DB exists'
        exit(0)
    else:
        print 'Creating DB'
        exit(1)


