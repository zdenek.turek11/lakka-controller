import React from "react";
import Navbar from "react-bootstrap/Navbar";
import {ModuleList} from "../../ModuleList";
import {FaQuestionCircle, FaCheckCircle, FaTimesCircle} from "react-icons/fa";
import {IconContext} from "react-icons";
import {OverlayTrigger, Tooltip} from "react-bootstrap";
import {FiLoader} from "react-icons/fi";

export default class StatusInfo extends React.Component{

    constructor(props) {
        props['modules'] = ModuleList;
        super(props);
        this.state = {
            modules: this.props.modules.map((module) => {
                return {
                    name: module.name,
                    status: 0,
                    checked: false,
                }
            })
        }
    }

    componentDidMount() {
        setInterval(async () => {
            let currentState = {modules: []};

            await Promise.all(this.props.modules.map(async (module) => {
                let status = await module.check();
                currentState.modules.push({
                    name: module.name,
                    checked: true,
                    status: status,
                });
            }));

            this.setState(currentState);
        }, 5000);
    }

    render() {
        return <Navbar.Text>
            {this.state.modules.map((module) => {
                let icon = <FaQuestionCircle/>;
                let color = 'gray';
                if(module.checked) {
                    switch (module.status) {
                        case 0:
                            icon = <FaCheckCircle />;
                            color = 'green';
                            break;
                        case -1:
                            icon = <FaTimesCircle />;
                            color = 'red';
                            break;
                        case 1:
                            icon = <FiLoader />;
                            color = 'yellow';
                            break;
                        default:
                            break;
                    }
                }
                let iconElement = (
                    <IconContext.Provider value={
                        {
                            color: color
                        }
                    } >
                        {icon}
                    </IconContext.Provider>
                );
                return (
                    <OverlayTrigger overlay={
                        <Tooltip>
                            {module.name}
                        </Tooltip>
                    } placement="bottom">
                        <div>
                            {iconElement}
                        </div>
                    </OverlayTrigger>
                );
            })}

        </Navbar.Text>;
    }
}