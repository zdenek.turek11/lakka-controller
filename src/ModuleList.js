import Backend from "./Backend";

export const ModuleList = [
    {
        check: () => {
            return Backend.check();
        },
        name: 'Backend',
    }
];