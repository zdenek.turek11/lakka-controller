export default class Backend {

    static async sendRequest(command, data = {}) {
        try {
            let response = await fetch(process.env.PUBLIC_URL + '/cgi-bin/process.py', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({command: command, data: data})
            });
            return response.json();
        } catch (e) {
            return {
                status: -1,
                message: 'Backend not available',
                data: {}
            };
        }

    }

    static async check() {
        let response = await this.sendRequest('check');
        return response.status;
    }
}