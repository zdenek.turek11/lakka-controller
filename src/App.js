import React from 'react';
import './App.css';
import {
    HashRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { IndexLinkContainer } from "react-router-bootstrap";
import {NavItem, NavLink} from "react-bootstrap";
import NavbarToggle from "react-bootstrap/NavbarToggle";
import NavbarCollapse from "react-bootstrap/NavbarCollapse";
import {Games} from "./Pages/Games/Games";
import StatusInfo from "./Components/StatusInfo/StatusInfo";


function App() {
  return (
    <div className="App">
        <Router>
            <Navbar bg="dark" expand="lg" className="navbar-dark">
                <NavbarToggle aria-controls="basic-navbar-nav" />
                <NavbarCollapse id="basic-navbar-nav">
                    <Nav justify defaultActiveKey={"/"} as="ul">
                        <NavItem as="li">
                            <IndexLinkContainer to="/">
                                <NavLink>Home</NavLink>
                            </IndexLinkContainer>
                        </NavItem>
                        <NavItem as="li">
                            <IndexLinkContainer to="/games">
                                <NavLink>Games</NavLink>
                            </IndexLinkContainer>
                        </NavItem>
                    </Nav>
                </NavbarCollapse>
                <StatusInfo/>
            </Navbar>

            <Switch>
                <Route path="/games">
                    <Games/>
                </Route>
                <Route path="/profiles">
                    Profiles
                </Route>
                <Route exact path="/">
                    Homepage
                </Route>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
