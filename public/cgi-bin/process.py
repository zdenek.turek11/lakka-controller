#!/usr/bin/python2.7

import json
import sys

post = json.loads(sys.stdin.read())

output = {
    'status': 0,
    'message': '',
    'data': {},
}

if post['command'] == 'check':
    output['status'] = 0
    output['message'] = 'Ok'
elif post['command'] == 'checkDB':
    output['status'] = 2
    output['message'] = 'Not yet implemented'
else:
    output['status'] = -1
    output['message'] = 'Invalid command!'


print

print json.dumps(output)